# Quick Start Guide

* Clone or fork Softlimit Client Starter: `git clone git@bitbucket.org:softlimit/softlimit-client-starter.git`

* `git submodule update --init` - updates the softlimit-framework submodule

* `npm run installdep` - replaces/bundles the standard `npm install` command

* Verify that your node version is at least 14: `node -v`

* `npm run build`


# Main Concepts
There are 2 repos: **softlimit-framework** and **softlimit-client-starter**

softlimit-client-starter can be forked to start dev for a new client build
the client repo contains a submodule reference to the softlimit-framework (master branch) repo. Updates to the framework can be pulled in through a submodule update.

Both Client and Softlimit-framework contain a webpack.config.js file:
client/webpack.config.js imports all of the relevant options from Softlimit framework's file
There are still some duplicate things here related to imports

* `npm run build` -- builds the current folder to /dist
* `npm run deploy` -- deploys and builds to the development theme defined in config.yml
* `npm run watch` -- watches for changes and uploads changes to the /dist folder to the development theme
* `npm run start` -- builds, uploads, and watches for and uploads changes to the /dist folder to the development theme
* `npm run build-production` -- builds with minified JS and css for production deployment
* `npm run deploy-production` -- link npm run deploy but is minified - this will be used in the pipeline, uses production theme in config.yml

There are 4 main JS and SCSS files that are loaded/pre-loaded conditionally
main.js & main.scss (the theme that is always loaded)
collection.js & collection.scss (collection pages)
product.js & product.scss (product pages)
account.js & account.scss (account pages)

# Softlimit-framework
#### /required
This folder contains all of the required files for the base theme. All builds start from all files in this folder.

All the folders outside of the /required folder contain elements for "modular" components that can be deployed to a client build by including the component and its relevant dependencies. The dependencies are defined inside a single JS file for each component. For example /scripts/components/infinite_scroll.js contains:

`import 'Softlimit/templates/collection.infinite.liquid'`
`import 'Softlimit/styles/components/infinite-scroll.scss'`

This means the "collection.infinite" template and infinite-scroll stylesheet will be included in any build that imports the infinite_scroll.js Object. More details on this in the Client section

#### /required/styles/critical.scss
This file is rendered and output (based on /required/render/criticalcss.pug) on build to /dist/snippets/criticalcss.liquid

This is included in the <head> of the theme file and should be only the most necessary CSS for the "above the fold" page paint.

# Client-starter
**All theme based client work happens in client folders**
Any liquid files in these folders will REPLACE any liquid files of the same name and path from the softlimit-framework/required folder during the build process
In order to not have to replace many important section files we can make use of some snippets we can call HOOKS (borrowing from the wordpress world)

* `npm run installdep` -- installs dependencies for both client and submodule softlimit-framework

These hooks exist:
/snippets/hook-body-end-scripts.liquid (inserts at the end of the <body> tag)
/snippets/hook-product-top.liquid (inserts before product section)
/snippets/hook-product-bottom.liquid (inserts after product section)

We can also replace pre-defined icon files. They can include inlined SVGs or any HTML really
/snippets/icon-arrow-down.liquid
/snippets/icon-close.liquid
/snippets/icon-delete.liquid
/snippets/icon-minus.liquid
/snippets/icon-plus.liquid

The styles in client/src/styles are built by webpack into the appropriate places, and are imported in the JS files in /scripts. For example in product.js:
import 'SLReq/scripts/product.js' #Softlimit Required folder product section JS, which loads product required styles
import 'Client/styles/product.scss' #Client product styles, appended to softlimit required styles

Each of the main stylesheets are extended this way, including critical.scss

## Images
Responsive Image snippet should be familiar, a new parameter "tinyImgUrl" will set the image source to be a 4px image in the assets folder if "tinyImgUrl = true". This only prevents a bunch of calls to the server for small images on a collection page or something.

Native browser lazyloading is implemented, with a fallback for lazysizes

## Ajax Cart
**/snippets/ajax-cart-template.liquid**
Defines the layout script for the Ajax Cart. There are a few required strings in here to make everything fit together. So this may not be the best way for this at the moment. Hoping to avoid handlebars.js

## Styles
Autoprefixer is running, so we do not need to use vendor prefixes

## Locales
All locales from the Required folder are prefixed with "sl." so that we can define client strings separately. Still need to figure out the build process for concatenating the client locale files with these defaults that have been adapted from Slate.

## Quantity selectors
Any div or input with "data-quantity" will be turned into a JS quantity selector.
Possible that this should be changed to any number input

## Buttons
Buttons have methods defined for any 'button, .button', that can be called in any JS on that button:

`button.addLoading()`
`button.removeLoading()`
`button.addError()`
`button.removeError()`
`button.addSuccess()`
`button.removeSuccess()`

Mostly these just add and remove classes "success", "error", "loading", but also just manage that in general. These can be styled, etc per client.