var ENV = process.env.NODE_ENV

const webpack = require('webpack')
const path = require('path')
const {
  CleanWebpackPlugin
} = require('clean-webpack-plugin')


const fs = require('fs')

class MergeJSONSchema {
  constructor(options = {}) {
      this.src = options.src
      this.destDir = options.destDir
      this.destFile = options.destFile
    }
    apply(compiler) {
      var JSONoutput = []
      const mergeJSONs = this.src
      mergeJSONs.forEach((filename) => {
        const contents = JSON.parse(fs.readFileSync(filename, 'utf8'));
        JSONoutput = JSONoutput.concat(contents)
      })
      //make dist config directory
      fs.mkdir(this.destDir, {
        recursive: true
      }, (err) => {
        if (err) throw err;

        fs.writeFileSync(path.resolve(this.destDir,this.destFile), JSON.stringify(JSONoutput))
      })
    }
}

const ClientOptions = require('./softlimit-framework/webpack.config.js')

ClientOptions.output.path = path.resolve(__dirname, 'dist')

ClientOptions.resolve = {
  alias: {
    SLExtensions: path.resolve(__dirname, './softlimit-framework/src/_extensions'),
    SLFramework: path.resolve(__dirname, './softlimit-framework/src/'),
    Client: path.resolve(__dirname, 'src/'),
  }
}

ClientOptions.entry = {
  main: ['Client/scripts/main.js', 'Client/styles/main.scss'],
  account: ['Client/scripts/account.js', 'Client/styles/account.scss'],
  collection: ['Client/scripts/collection.js', 'Client/styles/collection.scss'],
  product: ['Client/scripts/product.js', 'Client/styles/product.scss'],
  critical: 'Client/scripts/criticalcss.js'
}

ClientOptions.plugins.push(new MergeJSONSchema({
  src: [path.resolve(__dirname,'./softlimit-framework/src/config/settings_schema.json'), path.resolve(__dirname,'./src/config/settings_schema.json')],
  destDir: __dirname + '/dist/config/',
  destFile: 'settings_schema.json'
}))

module.exports = ClientOptions
