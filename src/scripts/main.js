import _ from 'lodash'

//SOFTLIMIT THEME DEPENDENCY
import 'SLFramework/scripts/required.js'

//Overrides with any client template files
import './client-required.js'

//LIST CLIENT AVAILABLE MODULES
import {clientModule} from 'SLExtensions/module.js'
clientModule()
