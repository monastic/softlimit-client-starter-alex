/*
  WE NEED TO CONCAT CLIENT STRING LOCALE AND SETTINGS_SCHEMA TO THE DEFAULT (namespaced) FILES
*/

function requireAll(r) { r.keys().forEach(r); }
requireAll(require.context('Client', true, /\.liquid$/));

/*
Client settings schema is imported and merged in a custom Webpack plugin
import 'Client/config/settings_schema.json'
*/
