/*
  this is a definitions file to build from single variables across JS and SCSS
  These are not updated during WATCH
*/
module.exports = {
  base: "16px",
  spacing: "1rem",
  typography: {
    font: "'Open Sans', sans-serif",
    text: "1rem",
    title: "2rem"
  },
  breakpoints: {
    small: "900px",
    medium: "1200px",
    large: "1500px"
  },
  colors: {
    primary: "#2c97de",
    secondary: "#7F8FA4",
    lightgrey: "#F5F8FA",
    warning: "#f2c500",
    success: "#1fce6d",
    danger: "#e94b35",
    error: "#e94b35"
  }
};
